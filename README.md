## Dependencies

* [git](https://git-scm.com/downloads)

## Installation

1. `git clone git@gitlab.com:jsierrabravo/data_reader.git`
2. `cd data_reader`
3. `pip install -r requirements.txt`
4. `python3 manage.py migrate`
5. `python manage.py runserver`
6. Go to `http://127.0.0.1:8000/`