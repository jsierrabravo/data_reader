from django.urls import path

from registers.views import upload_file
from registers.views import xlsx_registers_list, xlsx_statistics
from registers.views import csv_registers_list, csv_statistics

urlpatterns = [
    path('', upload_file, name='upload_file'),
    path('xlsx_registers_list', xlsx_registers_list, name='xlsx_registers_list'),
    path('xlsx_statistics', xlsx_statistics, name='xlsx_statistics'),
    path('csv_registers_list', csv_registers_list, name='csv_registers_list'),
    path('csv_statistics', csv_statistics, name='csv_statistics'),
]
