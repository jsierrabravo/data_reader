from django.db import models


# Create your models here.
class XlsxData(models.Model):
    class Meta:
        db_table = 'xlsx_data'

    region = models.CharField(max_length=60)
    country = models.CharField(max_length=50)
    item_type = models.CharField(max_length=20)
    sales_chanel = models.CharField(max_length=10)
    order_priority = models.CharField(max_length=1)
    order_date = models.DateField()
    order_id = models.IntegerField()
    ship_date = models.DateField()
    units_sold = models.IntegerField()
    unit_price = models.FloatField()
    unit_cost = models.FloatField()
    total_revenue = models.FloatField()
    total_cost = models.FloatField()
    total_profit = models.FloatField()


class CsvData(models.Model):
    class Meta:
        db_table = 'csv_data'

    date = models.DateField()
    description = models.CharField(max_length=50)
    deposits = models.FloatField()
    withdrawls = models.FloatField()
    balance = models.FloatField()
