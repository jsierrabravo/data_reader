import math
from datetime import datetime
import pandas as pd


def validate_str(value, max_length=math.inf):
    """ Validate if the input value can be parsed to string

        :param value: value to be validated
        :type value: string
        :param max_length: it specifies the maximum amount of characters that the input value can have
        :type max_length: int

        :return is_valid: True if the input value can be parsed to string, False if not
        :rtype is_valid: bool
        :return value_str: if is_valid is True, it contains the string of the input. If not it is None
        :rtype value_str: string
    """

    value_str = None
    is_valid = False

    if not str(value) == 'nan':

        try:
            value_str = str(value)
            if len(value_str) <= max_length:
                is_valid = True
        except TypeError:
            pass

    return is_valid, value_str


def validate_int(value):
    """ Validate if the input value can be parsed to integer

        :param value: value to be validated
        :type value: integer or integer-like string

        :return is_valid: True if the input value can be parsed to integer, False if not
        :rtype is_valid: bool
        :return value_int: if is_valid is True, it contains the integer parsed from the input. If not it is None
        :rtype value_int: integer
    """

    value_int = None
    is_valid = False

    if not str(value) == 'nan':
        try:
            value_str = str(value).replace(',', '')
            value_int = int(value_str)
            is_valid = True
        except TypeError:
            pass

    return is_valid, value_int


def validate_float(value):
    """ Validate if the input value can be parsed to float

        :param value: value to be validated
        :type value: float or float-like string

        :return is_valid: True if the input value can be parsed to float, False if not
        :rtype is_valid: bool
        :return value_float: if is_valid is True, it contains the float parsed from the input. If not it is None
        :rtype value_float: float
    """

    value_float = None
    is_valid = False

    if not str(value) == 'nan':
        try:
            value_str = str(value).replace(',', '')
            value_float = float(value_str)
            is_valid = True
        except TypeError:
            pass

    return is_valid, value_float


def to_date(value, ext):
    """ Parses the input value to date depending on the file extension. It complements the validate_date function

    :param value: value to be parsed
    :type value: date or date-like string
    :param ext: file extension. Xlsx and Csv files have different date format, so, it is important to know the ext
    :type ext: string

    :return date_object: if is_valid is True, it contains the date in python date format. If not it is None
    :rtype date_object: date
    """

    if 'datetime' in str(type(value)):
        date_object = value
    elif 'int' in str(type(value)):
        date_object = datetime.fromordinal(datetime(1900, 1, 1).toordinal() + value - 2)
    else:
        if ext == '.xlsx':
            date_object = datetime.strptime(value, '%m/%d/%Y')
        else:
            date_object = datetime.strptime(value, '%d-%b-%Y')

    return date_object


def validate_date(value, ext):
    """ Validates if the input value can be parsed to date object

        :param value: value to be validated
        :type value: date or date-like string
        :param ext: file extension. Xlsx and Csv files have different date format, so, it is important to know the ext
        :type ext: string

        :return is_valid: True if the input value can be parsed to date, False if not
        :rtype is_valid: bool
        :return date_object: if is_valid is True, it contains the date in python date format. If not it is None
        :rtype date_object: date
    """

    date_object = None
    is_valid = False

    if not str(value) == 'nan':
        try:
            date_object = to_date(value, ext)
            is_valid = True
        except IndexError:
            pass
        except TypeError:
            pass
    return is_valid, date_object


def handle_validation(value, t, ext):
    """ Function to make it easier to validate if a value can be parsed to a specific type

        :param value: value to be validated
        :type value: date or date-like string
        :param t: desired type to parse to
        :type t: tuple or string
        :param ext: file extension. Xlsx and Csv files. Some functions need to know the ext
        :type ext: string

        :return is_valid: True if the input value can be parsed to the desired type, False if not
        :rtype is_valid: bool
        :return curated_value: if is_valid is True, it contains the parsed value to the desired type
        :rtype curated_value: string, integer, float or date
    """

    curated_value = None
    is_valid = False

    if t[0] == 'str':
        is_valid, curated_value = validate_str(value, t[1])
    elif t == 'int':
        is_valid, curated_value = validate_int(value)
    elif t == 'float':
        is_valid, curated_value = validate_float(value)
    elif t == 'date':
        is_valid, curated_value = validate_date(value, ext)

    return is_valid, curated_value


def validate_row(row, ext):
    """ Validates an entire row

        :param row: list of data contained in a row of the uploaded file
        :type row: list
        :param ext: file extension. Xlsx and Csv files have different columns
        :type ext: string

        :return is_valid: True if each value in the input row can be parsed to the desired type, False if not
        :rtype is_valid: bool
        :return curated_row: if is_valid is True, it contains each value in the row in the desired type
        :rtype curated_row: list
    """

    # The list "types" depends on the file extension. It contains the list of the desired types of each value of the row
    # It must be in the same order as defined in the respective model

    # If the desired type is "string", the corresponding element in the list must be a tuple of two elements:
    # The first one is the type (str, e.i., string), and the second one is the maximum length allowed by the model
    if ext == '.xlsx':
        types = [
            ('str', 60),
            ('str', 50),
            ('str', 20),
            ('str', 10),
            ('str', 1),
            'date',
            'int',
            'date',
            'int',
            'float',
            'float',
            'float',
            'float',
            'float',
        ]
    else:
        types = [
            'date',
            ('str', 50),
            'float',
            'float',
            'float',
        ]

    curated_row = []
    is_valid = False

    if len(types) == len(row):
        for i in range(len(row)):
            # Validating each colum of the input row
            is_valid, curated_value = handle_validation(row[i], types[i], ext)
            curated_row.append(curated_value)
            if not is_valid:
                # If at least one value is not valid, then the loop is broken
                break

    return is_valid, curated_row


def handle_uploaded_file(f, ext):
    """ Handles the first step after uploading a file depending on it extension

        :param f: uploaded file
        :type f: temporary file
        :param ext: file extension. Xlsx and Csv files must be managed in different ways
        :type ext: string

        :return data: contains a pandas dataframe with all the data in the upladed file
        :rtype data: DataFrame
    """
    if ext == '.xlsx':
        try:
            data = pd.read_excel(f.temporary_file_path())
        except AttributeError:
            data = pd.read_excel(f)
    else:
        try:
            data = pd.read_csv(f.temporary_file_path())
        except AttributeError:
            data = pd.read_csv(f)

    return data
