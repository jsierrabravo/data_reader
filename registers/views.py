import pathlib

from django.db.models import Count
from django.db.models.functions import TruncYear
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from .forms import UploadFileForm
from .models import XlsxData, CsvData
from .utils import handle_uploaded_file, validate_row


def upload_file(request):
    """
    A view to allow users to upload xlsx and csv files. Here, the info contained in the files is read, cleaned and sent
    to the respective database
    """

    # Initializing context
    context = {
        'form': UploadFileForm(),
    }

    # Allowed file extensions
    exts = ['.csv', '.xlsx']

    if request.method == 'POST':

        # Updated the form. Now, it contains the completed form
        form = UploadFileForm(request.POST, request.FILES)

        if form.is_valid():

            # Get the file
            uploaded_file = request.FILES['file']
            # Extract the file extension
            ext = pathlib.Path(uploaded_file.name).suffix

            # If the file extension is allowed:
            if ext in exts:

                # The utils.py file handles the file depending on the extension, and store the data in the
                # variable file_data
                file_data = handle_uploaded_file(uploaded_file, ext)

                if ext == '.xlsx':
                    model = XlsxData
                else:
                    model = CsvData

                # Get the list of fields of the model
                model_fields = [f.name for f in model._meta.get_fields()]

                # Iterate over each row in the uploaded file
                for i in range(len(file_data.index)):
                    row = file_data.loc[i].tolist()

                    # The function validate_row() outputs a boolean value which indicates if the data contained in the
                    # current row is valid to be stored in the database. Then, the curated data is stored in the
                    # variable curated_row
                    is_valid, curated_row = validate_row(row, ext)

                    if is_valid:
                        # Saving a valid row in the database
                        kwargs = dict(zip(model_fields[1:], curated_row))
                        model.objects.create(**kwargs)

                # The url to redirect after stored all valid rows in the database is managed in the redirect_to variable
                # It depends on the file extension
                redirect_to = ext[1:] + '_registers_list'

                return HttpResponseRedirect(redirect_to)

            else:

                # If the uploaded file is not valid, then the form is cleaned
                context['form'] = UploadFileForm()

        else:

            # If the submitted form is not valid, then it is cleaned
            context['form'] = UploadFileForm()

    return render(request, 'upload_file.html', context)


def xlsx_registers_list(request):
    model = XlsxData

    # List of all registers in the xlsx database. It is sent via the context
    all_registers = model.objects.all()
    all_registers_list = list(all_registers)

    context = {
        'ftype': 'xlsx',
        'items': all_registers_list,
    }

    return render(request, 'registers_list.html', context)


def csv_registers_list(request):
    model = CsvData

    # List of all registers in the csv database. It is sent via the context
    all_registers = model.objects.all()
    all_registers_list = list(all_registers)

    context = {
        'ftype': 'csv',
        'items': all_registers_list
    }

    return render(request, 'registers_list.html', context)


def xlsx_statistics(request):
    model = XlsxData

    # List of all registers in the xlsx database
    all_registers = model.objects.all()
    all_registers_list = list(all_registers)

    # Query 1: Amount of imported rows
    total_rows = len(all_registers_list)

    # Query 2: Item types contained in the file according to the column ITEM TYPE
    item_types = model.objects.order_by().values_list('item_type').distinct()
    item_types_list = [item[0] for item in item_types]

    # Query 3: How many regions are contained in the file and what are they? (see the column REGION)
    regions = model.objects.order_by().values_list('region').distinct()
    regions_list = [item[0] for item in regions]
    total_regions = len(regions_list)

    # Query 4: Get the amount of orders by priority (see column ORDER PRIORITY)
    orders_by_priority = model.objects.values('order_priority').annotate(count=Count('order_priority'))
    orders_by_priority_list = list(orders_by_priority)

    # Query 5: Get the amount of orders by item type (see column ITEM TYPE)
    orders_by_item_type = model.objects.values('item_type').annotate(count=Count('item_type'))
    orders_by_item_type_list = list(orders_by_item_type)

    # Sending the queries to the html template
    context = {
        'ftype': 'xlsx',
        'total_rows': total_rows,
        'item_types': item_types_list,
        'total_regions': total_regions,
        'regions': regions_list,
        'orders_by_priority': orders_by_priority_list,
        'orders_by_item_type': orders_by_item_type_list,
    }

    return render(request, 'statistics.html', context)


def csv_statistics(request):
    model = CsvData

    all_registers = model.objects.all()

    # Query 1: Amount of imported rows
    all_registers_list = list(all_registers)
    total_rows = len(all_registers_list)

    # Query 2: Amount of movements by description in the file
    transactions_by_desc = model.objects.values('description').annotate(count=Count('description'))
    transactions_by_desc_list = list(transactions_by_desc)

    # Query 3: Amount of transactions per year (see column DATE)
    transactions_per_year = model.objects.annotate(year=TruncYear('date')).values('year').annotate(count=Count('id'))
    transactions_per_year_list = list(transactions_per_year)

    # Sending the queries to the html template
    context = {
        'ftype': 'csv',
        'total_rows': total_rows,
        'transactions_by_desc': transactions_by_desc_list,
        'transactions_per_year': transactions_per_year_list
    }

    return render(request, 'statistics.html', context)
