from django.contrib import admin

# Register your models here.
from registers.models import XlsxData, CsvData

admin.site.register(XlsxData)
admin.site.register(CsvData)